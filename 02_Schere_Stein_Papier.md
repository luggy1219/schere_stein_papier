# Schere, Stein, Papier

> Schere, Stein, Papier ist ein sowohl bei Kindern als auch Erwachsenen beliebtes und weltweit verbreitetes Spiel. Zwei Spieler wählen je eins der drei möglichen Symbole Schere, Stein oder Papier und zeigen dieses dann auf ein Kommando gleichzeitig (schlagartig) mit Hilfe einer ihrer Hände an. Da jedes Symbol gegen ein anderes gewinnen oder verlieren kann, steht immer einer der Spieler als Gewinner fest. Zu Unentschieden kommt es, wenn beide Spieler dasselbe Symbol wählen. Das Spiel wird in diesem Fall wiederholt.

Wir wollen ein Schere, Stein, Papier Spiel in React implementieren, wobei der Benutzer geben den Computer antritt.

Am Spiel-Bildschirm werden dabei drei Buttons angezeigt. Je einer für Schere, Stein, und Papier. Im Hintergrund hat sich das Spiel bereits errechnet mit welcher Option es antreten wird. Der Spieler muss nur noch einen Button drücken um zu wählen.

Zudem wird unterhalb der Buttons ein Punktestandstand angezeigt. Der Punktestand zählt die gewonnen Spiele des Spielers vs. die gewonnen Spiele des Computers.

Folgenden Szenarien können beim Spielen entstehen

- Computer gewinnt → Punktestand vom Computer wird erhöht
- Spieler gewinnt → Punktestand vom Spieler wird erhöht
- Gleiches Symbol → Spiel wird wiederholt
